<?php

namespace App\helper;

require '../config/init.php';

use Exception;
use GuzzleHttp\Client;

class ApiClient {

    private $apiClient;

    public function getApiClient(string $type)
    {
        if($type === 'sms')
        {
            $this->apiClient = new Client([ 
                'base_uri' => $_ENV['SMS_URI'],
                'verify'=> false,
                'http_errors' => false,
            ]);
        }
        elseif($type === 'lms')
        {
            $this->apiClient = new Client([ 
                'base_uri' => $_ENV['BASE_URI'],
                'verify'=> false,
                'http_errors' => false,
            ]);
        } else {
            throw new Exception("Invalid API type: $type");
        }

        return $this->apiClient;
    }
}
