<?php

namespace App\helper;

use DateTime;
require '../config/init.php';

class Helper{

    public static function isValidDate($start_date , $end_date)
    {
        $current_date = new DateTime();
        $start = new DateTime($start_date);
        $end = new DateTime($end_date);

        return $current_date >= $start && $current_date <= $end ? true : false;
    }

}

