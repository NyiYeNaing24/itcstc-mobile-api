<?php

namespace App\helper;

class ApiResponse
{
    public static function handle($statusCode , $data)
    {
        switch ($statusCode) {
            case '200':
                return self::successResponse($data);
                break;
            case '400':
                return self::badRequestResponse($data);
                break;
            case '404':
                return self::notFoundResponse();
                break;
            default:
                return;
                break;
        }
    }
    public function successResponse($data)
    {
        http_response_code(200);
        return json_encode([
            'code' => 200,
            'success' => true,
            'message' => 'success',
            'data' => $data,
        ]);
    }

    public function badRequestResponse($message)
    {
        http_response_code(400);
        return json_encode([ 
            'code' => 400,
            'success' => false,
            'message' => $message,
        ]);
    }

    public static function notFoundResponse()
    {
        http_response_code(404);
        return json_encode([ 
            'code' => 404,
            'success' => false,
            'message' => "No data found!",
            'data' => [] 
        ]);
    }
}
