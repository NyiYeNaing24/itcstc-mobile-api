<?php

namespace App\service;

require '../config/init.php';

use PDO;
use App\helper\ApiClient;
use App\helper\ApiResponse;
use App\helper\Helper;

class DashboardService {

    private $sms_client;

    private $lms_client;

    private $sms_token;

    private $lms_token;

    private $conn;

    public function __construct(ApiClient $api_client)
    {
        $this->sms_client = $api_client->getApiClient('sms');        
        $this->lms_client = $api_client->getApiClient('lms');        
    }

    public function setConnection($conn)
    {
        $this->conn = $conn; 
    }

    public function setLmsToken($lms_token)
    {
        $this->lms_token = $lms_token;
    }

    public function setSmsToken($sms_token)
    {

        $this->sms_token = $sms_token;
    }

    public function getEnrolledCourses($response)
    {
        $course_arr = array_filter($response->relationships->courses , function($courses) {
            $validDate = Helper::isValidDate($courses->attributes->start_date, $courses->attributes->end_date);
            return ($validDate && $courses->status === 'selected');
        });

        return array_values($course_arr);
    }

    public function getCourseDetails($courses)
    {
        $response = $this->lms_client->request('GET' , 'core_course_get_courses' , [
            "headers" => [
                "Authorization" => $_ENV['ADMIN_TOKEN'],
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ],
            "json" => [
                "options" => [
                    "ids" => [$courses[0]->course_key]
                ]  
            ]
        ])->getBody();  

        return json_decode($response);
    }

    public function getAssignments($courses)
    {
        $response = $this->lms_client->request('GET' , 'mod_assign_get_assignments' , [
            "headers" => [
                "Authorization" => $this->lms_token,
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ],
            "json" => [
                "courseids" => [
                    $courses[0]->course_key
                ],
                "includenotenrolledcourses" => 1
            ]
        ])->getBody();  

        $assignment = json_decode($response);

        return $assignment->courses[0]->assignments;
    }

    public function getSubmittedAssignments($user_id , $course_id)
    {
        $query = "SELECT assign.id AS assignment_id , course.id AS course_id , 
        assign.name AS assignment_name , course.fullname AS course_name, assign_sub.status
        FROM itcstc_course AS course 
        JOIN itcstc_assign AS assign ON course.id = assign.course
        JOIN itcstc_assign_submission AS assign_sub ON assign.id = assign_sub.assignment
        WHERE course.id = :course_id AND assign_sub.userid = :user_id AND assign_sub.status = 'submitted'"; 

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':course_id' , $course_id , PDO::PARAM_INT);
        $stmt->bindParam(':user_id' , $user_id , PDO::PARAM_INT);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $assignments = array();

        foreach ($rows as $key => $value) {
            $assignments[$key] = $value;
        }

        return $assignments;
    }

    public function getAssignmentSubmissionStatus($course , $userid)
    {
        $assignments = self::getAssignments($course);
        $submitted_assignments = self::getSubmittedAssignments($userid , $course[0]->course_key)[0];

        $format_assignments = array();

        foreach ($assignments as $key => $assignment) {
            $assignment->id == $submitted_assignments['assignment_id'] ?
            $assignment->status = 'Submitted' :
            $assignment->status = 'Not Submitted';

            array_push($format_assignments , $assignment);
        }

        return $format_assignments;
    }

    public function getQuizes($courses)
    {
        $response = $this->lms_client->request('GET' , 'mod_quiz_get_quizzes_by_courses' , [
            "headers" => [
                "Authorization" => $this->lms_token,
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ],
            "json" => [
                "courseids" => [
                    $courses[0]->course_key
                ],
            ]
        ])->getBody();  

        return json_decode($response);
    }

    public function getUserProfile()
    {
        $response = $this->sms_client->request('GET' , 'user-profile' , [
            "headers" => [
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $this->sms_token
            ],
        ])->getBody();

        return json_decode($response);
    }

    public function getLmsUser()
    {
        $response = $this->lms_client->request('GET' , 'core_webservice_get_site_info' , [
            "headers" => [
                "Authorization" => $this->lms_token,
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ],
        ])->getBody(); 

        return json_decode($response);
    }

    public function getGradings()
    {
        $gradings = [
            ['grade'=> 'A+' , 'marks' => '80 marks', 'operator' => '>=' , 'color' => '#10b94f' , 'remark' => 'Excellent'], 
            ['grade'=> 'A' , 'marks' => '60 marks', 'operator'=> '>=' , 'color' => '#93c94d' , 'remark' => 'Very Good'], 
            ['grade'=> 'B+' , 'marks' => '50 marks', 'operator'=> '>=' , 'color' => '#fee331' , 'remark' => 'Good'], 
            ['grade'=> 'B' , 'marks' => '50 marks', 'operator' => '', 'color' => '#F89828' , 'remark' => 'Average'], 
            ['grade'=> 'C' , 'marks' => '50 marks' , 'operator' => '<', 'color' => '#FF3E41' , 'remark' => 'Below Average'], 
        ];

        return $gradings;
    }

    public function getScoringRule($course_id)
    {
        $query = "SELECT * FROM itcstc_course WHERE id = :course_id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':course_id' , $course_id , PDO::PARAM_INT);
        $stmt->execute();

        $rows = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return [
            'scroing_exam' => $rows['scoringexam'],
            'scoring_assign'=> $rows['scoringassign'],
            'scoring_quizz' => $rows['scoringquiz'],
            'scoring_discipline' => $rows['scoringdiscipline']
        ];
    }

    public function courseOverview($course_id , $user_id)
    {
        $module_sql = "SELECT count(*) as total FROM itcstc_course_modules cm WHERE cm.course = :course_id AND cm.completion = 1";
        $stmt = $this->conn->prepare($module_sql);
        $stmt->bindParam(':course_id' , $course_id , PDO::PARAM_INT);
        $stmt->execute();

        $modules = $stmt->fetch(PDO::FETCH_ASSOC);

        $commodule_sql = "SELECT count(*) as total FROM itcstc_course_modules_completion cmc , itcstc_course_modules cm 
                         WHERE cmc.userid = :user_id AND cmc.completionstate = 1 AND cmc.coursemoduleid = cm.id AND cm.course = :course_id";

        $stmt = $this->conn->prepare($commodule_sql);
        $stmt->bindParam(':course_id' , $course_id , PDO::PARAM_INT);
        $stmt->bindParam(':user_id' , $user_id , PDO::PARAM_INT);
        $stmt->execute();

        $commodules = $stmt->fetch(PDO::FETCH_ASSOC);

        $total_modules = $modules['total'] === 0 ?? 1;
        $total_commodules = $commodules['total'];

        return ($total_commodules / $total_modules) * 100;
    }

    public function dashboardInfo($sms_user_profile) {

        $enrolled_courses = self::getEnrolledCourses($sms_user_profile);
        $gradings = self::getGradings();

        if(!empty($enrolled_courses))
        {
            $course_details = self::getCourseDetails($enrolled_courses);

            $assignments = self::getAssignmentSubmissionStatus($enrolled_courses , self::getLmsUser()->userid);
            $course_details[0]->assignments = $assignments;
            $quizzes = self::getQuizes($enrolled_courses);
            $course_details[0]->quizzes = $quizzes;

            $course_overview = self::courseOverview($enrolled_courses[0]->course_key , self::getLmsUser()->userid);
            $course_details[0]->course_overview = $course_overview;

            $scoring_rule = self::getScoringRule($enrolled_courses[0]->course_key);

            return [ 
                "profile" => self::getLmsUser(),
                "course" => $course_details,
                'gradings'=> $gradings,
                'socring_rules'=> $scoring_rule
            ];
        }

        return [ 
            "profile" => self::getLmsUser(),
            "course" => [],
            'gradings'=> $gradings,
            'socring_rules'=> []
        ];
    }
}

