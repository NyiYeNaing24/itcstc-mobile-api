<?php

namespace App\api;

require '../config/init.php';

use App\helper\ApiResponse;
use App\service\DashboardService;
use App\helper\ApiClient;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Accept: application/json');

$request = json_decode(file_get_contents("php://input"));
$sms_token = $request->sms_token ?? $request->sms_token;
$lms_token = $request->lms_token ?? $request->lms_token;

$api_client = new ApiClient();
$dashboard_service = new DashboardService($api_client);
$dashboard_service->setConnection($conn);
$dashboard_service->setLmsToken($lms_token);
$dashboard_service->setSmsToken($sms_token);

$user_profile = $dashboard_service->getUserProfile();
$user_info = $dashboard_service->dashboardInfo($user_profile);

if(empty($user_info))
{
    echo ApiResponse::handle(404, $user_info);
}

echo ApiResponse::handle(200, $user_info);