<?php

namespace App\api;

require '../config/init.php';

use GuzzleHttp\Client;
use App\helper\ApiResponse;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Accept: application/json');

$request = json_decode(file_get_contents("php://input"));
$username = $request->username ?? $request->username;
$password = $request->password ?? $request->password;

$client = new Client([
    'base_uri' => "https://uat-portal.itcstc.edu.mm/itcstclms/",
    'verify'=> false
]);

$response = $client->request('POST' , 'login/token.php?service=mobile_app_service' , [
    'form_params' => [
        'username' => $username,
        'password' => $password
    ]
]);

echo ApiResponse::handle($response->getStatusCode(), json_decode($response->getBody()));

