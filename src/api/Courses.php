<?php

namespace App\api;

require '../config/init.php';

use GuzzleHttp\Client;
use App\helper\ApiResponse;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Accept: application/json');

$request = json_decode(file_get_contents("php://input"));
$userid = $request->userid ?? $request->userid;
$token = $request->token ?? $request->token;

$client = new Client([
    'base_uri' => $_ENV['BASE_URI'],
    'verify'=> false,
    'http_errors' => false
]);

$response = $client->request('POST' , 'core_enrol_get_users_courses' , [
    "headers" => [
        "Authorization" => $token,
        "Accept" => "application/json",
        "Content-Type" => "application/json"
    ],
    'json' => [
        "userid" =>  $userid
    ]
]);

echo ApiResponse::handle($response->getStatusCode(), json_decode($response->getBody()));
