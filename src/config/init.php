<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/itcstcmobileapi/vendor/autoload.php';

use Dotenv\Dotenv;
use App\config\DB;

$dotenv = Dotenv::createImmutable($_SERVER['DOCUMENT_ROOT'] . '/itcstcmobileapi');
$dotenv->load();

$db = new DB();
$conn = $db->getConnection();



