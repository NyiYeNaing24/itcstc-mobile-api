<?php

namespace App\config;

require_once $_SERVER['DOCUMENT_ROOT'] . '/itcstcmobileapi/vendor/autoload.php';

use PDO;
use PDOException;

class DB {

    private $dbname;
    private $username;
    private $password;
    private $conn;

    public function __construct()
    {
        $this->dbname = $_ENV['DB_NAME'];
        $this->username = $_ENV['DB_USER'];
        $this->password = $_ENV['DB_PASSWORD'];
        
        try {
            $this->conn = new \PDO("mysql:host=localhost;dbname=" . $this->dbname, $this->username, $this->password);
            $this->conn->exec("set names utf8");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,true);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
        } catch (PDOException $e) {
            echo 'Error connecting to database: ' . $e->getMessage();
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }
}
